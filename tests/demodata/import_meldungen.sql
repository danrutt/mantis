INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder)
VALUES (2, '2020-09-15', '2021-01-28', '2021-06-07', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (3, '2021-02-07', '2021-02-07', '2021-06-07', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (4, '2020-08-14', '2021-02-14', '2021-06-07', 'F', 0, 1, 0, 0, 0, 'Terrarianer');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (5, '2020-08-23', '2021-03-06', '2021-06-07', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (6, '2020-09-06', '2021-03-15', '2021-06-07', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (8, '2021-03-31', '2021-03-31', '2021-06-07', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (9, '2021-04-24', '2021-04-24', '2021-06-07', 'F', 1, 0, 0, 0, 'Fundort nachgehakt, Fund im Garten, 0, ins Haus geholt und sehr früh geschlüpft');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (10, '2020-09-27', '2021-05-01', '2021-06-07', 'F', 0, 1, 0, 0, 0 'Karte geliefert, komplette Meldung liegt mir nicht vor, 0, Manfred nachgehakt 07.06.2021');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (12, '2021-04-20', '2021-06-09', '2021-06-10', 'F' ,0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (13, '2021-06-10', '2021-06-10', '2021-06-10', 'F', 0, 1, 0, 0, 0, 'Schlupf, 0, 2 Nymphen Fotobeleg');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (14, '2021-06-23', '2021-06-23', '2021-06-24', 'F', 0, 0, 1, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (15, '2021-06-26', '2021-06-26', '2021-07-20', 'F', 0, 0, 0, 1, 0, 'Koordinaten Manfred 20.07.2021 nachgehakt, 0, lt. Web-Telefonbuch: Im Brühl 18');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (16, '2021-06-26', '2021-06-26', '2021-06-28', 'F', 0, 0, 1, 0, 0, 'Funddaten siehe 2020');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (17, '2021-06-27', '2021-06-27', '2021-06-28', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (18, '2021-06-29', '2021-06-29', '2021-06-29', 'F', 1, 0, 0, 0, 0, 'Meldete bereits in den Vorjahren mit Fotos');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (19, '2021-07-01', '2021-07-02', '2021-07-20', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (20, '2021-07-03', '2021-07-03', '2021-07-20', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (21, '2021-07-07', '2021-07-07', '2021-07-28', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (22, '2021-07-09', '2021-07-09', '2021-07-20', 'F', 0, 1, 0, 0, 0, 'Fund im letzten Jahr am 26.08.2021 mit Adresse');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (23, '2021-07-11', '2021-07-11', '2021-07-20', 'F', 0, 1, 0, 0, 0, 'Fundort siehe 20220503 Zossen L1');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (24, '2021-07-11', '2021-07-11', '2021-07-20', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (25, '2021-07-12', '2021-07-12', '2021-07-28', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (26, '2021-07-17', '2021-07-17', '2021-07-20', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (27, '2021-07-18', '2021-07-19', '2021-07-28', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (28, '2021-07-16', '2021-07-21', '2021-07-26', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (29, '2021-07-20', '2021-07-22', '2021-07-28', 'F', 0, 1, 0, 0, 0, 'Koordinaten in Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (30, '2021-07-23', '2021-07-23', '2021-07-26', 'F', 0, 1, 0, 0, 0, 'Foto & Video (2verschiedene  Tiere)');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (31, '2021-07-24', '2021-07-24', '2021-07-26', 'F', 0, 0, 1, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (32, '2021-07-24', '2021-07-24', '2021-07-26', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (33, '2021-07-25', '2021-07-25', '2021-07-29', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (34, '2021-07-25', '2021-07-25', '2021-07-29', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (35, '2021-07-25', '2021-07-25', '2021-07-29', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (36, '2021-07-25', '2021-07-25', '2021-07-29', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (37, '2021-07-26', '2021-07-26', '2021-07-27', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (38, '2021-07-26', '2021-07-26', '2021-07-29', 'F', 0, 0, 1, 0, 0, 'Vereinshaus Mittelstraße 19, 0, rundherum keine Kleingärten zu sehen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (39, '2021-07-26', '2021-07-26', '2021-07-29', 'F', 0, 1, 0, 0, 0, 'besser noch einmal nachhaken (Koordinaten & Foto), 0, am 30.07. angeschrieben');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (40, '2021-07-27', '2021-07-28', '2021-07-29', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (41, '2021-07-28', '2021-07-28', '2021-07-29', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (42, '2021-07-27', '2021-07-28', '2021-07-29', 'F', 0, 0, 0, 1, 0, 'Spezialist, Mehrfachmelder 2020, 0, NABU Senftenberg');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (43, '2021-07-28', '2021-07-28', '2021-07-29', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (44, '2021-07-21', '2021-07-28', '2021-07-29', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (45, '2021-07-31', '2021-07-31', '2021-08-02', 'F', 0, 0, 0, 1, 0, 'Koordinaten gelifert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (46, '2021-08-03', '2021-08-03', '2021-08-04', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (47, '2021-07-24', '2021-08-03', '2021-08-04', 'F', 0, 1, 0, 0, 0, 'Koordinaten 2020 geliefert, 0, Meldungen auch 2020');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (48, '2021-08-03', '2021-08-03', '2021-08-04', 'F', 0, 0, 1, 0, 'Koordinaten geliefert, 0, 2020 schon einmal gemeldet');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (49, '2021-08-03', '2021-08-04', '2021-08-05', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (50, '2021-08-05', '2021-08-05', '2021-08-06', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (52, '2021-08-07', '2021-08-07', '2021-07-26', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (53, '2021-08-08', '2021-08-08', '2021-08-10', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (54, '2021-08-08', '2021-08-08', '2021-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (56, '2020-08-27', '2021-08-08', '2021-08-10', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (57, '2021-07-30', '2021-08-08', '2021-08-10', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (62, '2021-08-09', '2021-08-09', '2021-08-10', 'F', 0, 1, 0, 0, 0, 'telefonische Meldung');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (63, '2021-08-07', '2021-08-09', '2021-08-10', 'F', 1, 0, 0, 0, 0, 'sehr detaillierte Beschreibung');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (64, '2021-08-09', '2021-08-09', '2021-08-10', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (65, '2021-08-10', '2021-08-10', '2021-08-10', 'F', 1, 0, 0, 0, 0, '1 m & 1 w auf dem Foto, 0, Ralf nach den weiteren Einzelheiten angefragt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (66, '2021-08-12', '2021-08-12', '2021-08-13', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (68, '2021-08-13', '2021-08-13', '2021-08-17', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (69, '2021-08-13', '2021-08-13', '2021-08-17', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (70, '2021-08-14', '2021-08-14', '2021-08-18', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (71, '2021-08-14', '2021-08-14', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (72, '2021-08-14', '2021-08-14', '2021-08-18', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (73, '2021-08-14', '2021-08-14', '2021-08-18', 'F', 0, 0, 0, 1, 0, 'am 18.08. nachgehakt, 0, Koordinaten');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (74, '2021-08-14', '2021-08-14', '2021-08-18', 'F', 0, 0, 1, 0, 0, 'am 18.08. nachgehakt, 0, Koordinaten');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (75, '2021-08-14', '2021-08-14', '2021-08-18', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (76, '2021-08-13', '2021-08-14', '2021-08-18', 'F', 1, 0, 0, 0, 0, 'am 18.08. nachgehakt, 0, Koordinaten');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (77, '2021-08-14', '2021-08-14', '2021-08-17', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (78, '2021-08-14', '2021-08-14', '2021-08-18', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (79, '2021-08-14', '2021-08-14', '2021-08-18', 'F', 0, 0, 1, 0, 0, 'am 18.08. nachgehakt, 0, Koordinaten');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (80, '2021-08-13', '2021-08-14', '2021-08-17', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (81, '2021-08-14', '2021-08-14', '2021-08-18', 'F', 1, 0, 0, 0, 0, 'Bestimmung des Geschlechtes nach Erläuterung der Unterschiede');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (82, '2021-08-14', '2021-08-15', '2022-02-25', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (83, '2021-08-15', '2021-08-15', '2022-02-25', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (84, '2021-08-14', '2021-08-15', '2022-02-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (85, '2021-08-15', '2021-08-15', '2022-02-25', 'F', 0, 0, 1, 0, 0, 'siehe auch 20210723 Frauendorf');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (86, '2021-08-15', '2021-08-15', '2022-02-25', 'F', 0, 1, 0, 0, 0, 'Kartierungsbüro');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (87, '2021-08-15', '2021-08-15', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (88, '2021-08-15', '2021-08-15', '2022-02-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten gelilefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (89, '2021-06-27', '2021-08-15', '2022-02-25', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (90, '2021-08-04', '2021-08-15', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (91, '2021-08-15', '2021-08-15', '2022-02-25', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (92, '2021-08-14', '2021-08-15', '2022-02-25', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (93, '2021-08-14', '2021-08-15', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (94, '2021-08-14', '2021-08-15', '2022-02-25', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (95, '2021-08-08', '2021-08-15', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (96, '2021-08-16', '2021-08-16', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (97, '2021-08-16', '2021-08-16', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (98, '2021-08-16', '2021-08-16', '2022-02-25', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (99, '2021-08-16', '2021-08-16', '2022-02-25', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (100, '2021-08-14', '2021-08-16', '2022-02-25', 'F', 0, 0, 0, 0, 0, 'Koordinaten geliefert, 6 Exemplare und zahlreiche Ootheken, 3 männliche Nymphen + weibliche Imagines, 0, Nachwuchentomologe ORION Berlin');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (101, '2021-08-17', '2021-08-17', '2022-02-25', 'F', 0, 1, 0, 0, 0, 'Es soll Fotos geben, 0, liegen mir nicht vor; Artenkenner vom NABU');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (102, '2021-08-17', '2021-08-17', '2022-02-25', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (103, '2021-08-15', '2021-08-17', '2022-02-25', 'F', 0, 0, 0, 0, 0, 'Koordinaten geliefert, 1 Weibchen auf Foto, 0, 2 Tiere');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (104, '2021-08-17', '2021-08-17', '2022-02-25', 'F', 0, 0, 0, 0, 0, 'Koordinaten geliefert, Sicherer Fund, da in selber Meldung Foto von anderem Fundort, 0, keine Geschhlechtsangabe und keine Fotos');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (105, '2021-08-16', '2021-08-17', '2022-02-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (106, '2021-08-16', '2021-08-16', '2022-02-25', 'F', 1, 0, 0, 0, 'Lufthansa Technik Halle für die Koordinatenermittlung herangezogen, 0, keine genauen Angaben zur Halle von Melder gemacht');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (107, '2021-08-17', '2021-08-17', '2022-02-25', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (108, '2021-08-16', '2021-08-17', '2022-02-25', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (109, '2021-08-18', '2021-08-18', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (110, '2021-08-16', '2021-08-18', '2022-02-25', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (111, '2021-08-14', '2021-08-18', '2022-02-25', 'F', 1, 0, 0, 0, 0, 'noch einmal nachgehakt 25.02.2021');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (112, '2021-07-19', '2021-08-18', '2022-02-25', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (113, '2021-08-18', '2021-08-18', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (114, '2021-08-15', '2021-08-18', '2022-02-25', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (115, '2021-08-19', '2021-08-19', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (116, '2021-08-18', '2021-08-19', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (117, '2021-08-16', '2021-08-19', '2022-02-25', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (118, '2021-08-19', '2021-08-19', '2022-02-25', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (119, '2021-09-14', '2021-08-19', '2022-02-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (120, '2021-08-19', '2021-08-19', '2022-02-25', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (121, '2021-08-19', '2021-08-19', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (122, '2020-09-03', '2021-08-19', '2022-02-25', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (123, '2021-08-19', '2021-08-20', '2022-02-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (124, '2021-08-21', '2021-08-21', '2022-02-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert siehe 20210913 Cottbus 2');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (125, '2021-08-20', '2021-08-21', '2022-02-25', 'F', 0, 1, 0, 0, 0, 'Mehrfachmelderin');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (126, '2021-08-19', '2021-08-21', '2022-02-25', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (127, '2021-08-20', '2021-08-21', '2022-02-25', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (128, '2021-08-22', '2021-08-22', '2022-02-25', 'F', 1, 0, 0, 0, 0, 'am 25.02.2022 nachgehakt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (129, '2021-08-15', '2021-08-22', '2022-05-19', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (130, '2021-08-22', '2021-08-22', '2022-05-19', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (131, '2021-08-22', '2021-08-22', '2022-05-19', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (132, '2021-08-22', '2021-08-22', '2022-05-19', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (133, '2021-08-21', '2021-08-22', '2022-05-19', 'F', 0, 1, 0, 0, 0, 'am 19.05.2022 noch einmal nachgehakt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (135, '2021-08-14', '2021-08-22', '2022-05-25', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (136, '2021-08-24', '2021-08-24', '2022-05-25', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (137, '2021-08-24', '2021-08-24', '2022-05-25', 'F', 1, 0, 0, 0, 0, 'FO siehe Meldung 20200826 Nierstein');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (138, '2021-08-14', '2021-08-24', '2022-05-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (139, '2021-08-24', '2021-08-24', '2022-05-25', 'F', 0, 0, 0, 1, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (140, '2021-08-14', '2021-08-24', '2022-05-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (141, '2021-08-24', '2021-08-25', '2022-05-25', 'F', 0, 0, 1, 0, 0, 'bei den Manfreds nachgefragt 25.05.2022');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (142, '2021-08-24', '2021-08-25', '2022-05-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (143, '2021-08-25', '2021-08-25', '2022-05-25', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (144, '2021-08-25', '2021-08-25', '2022-05-25', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (145, '2021-08-25', '2021-08-25', '2022-05-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (146, '2021-08-26', '2021-08-26', '2022-05-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert (Koordinaten in den Bildeigenschaften passen nicht)');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (147, '2021-08-26', '2021-08-26', '2022-05-25', 'F', 0, 0, 1, 0, 0, 'siehe 20210826 Briesnig (Forst) ');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (148, '2021-08-26', '2021-08-26', '2022-05-25', 'F', 1, 0, 0, 0, 0, 'siehe 20210826 Briesnig (Forst) ');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (149, '2021-08-26', '2021-08-26', '2022-05-25', 'F', 0, 1, 0, 0, 0, 'siehe 20210826 Briesnig (Forst) ');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (150, '2021-08-25', '2021-08-26', '2022-05-25', 'F', 0, 0, 0, 1, 0, 'Koordinaten und Ortsbezeichnung stimmen nichtganz überein');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (151, '2021-07-31', '2021-08-26', '2022-05-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (152, '2021-08-10', '2021-08-26', '2022-05-25', 'F', 0, 1, 0, 0, 0, 'eine Meldung mit Foto, anhand der Beschreibung ringfingerdick ein Weibchen, 0, Wildwiese 50 m entfern');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (153, '2021-08-31', '2021-08-31', '2022-05-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (154, '2021-08-26', '2021-08-26', '2022-05-25', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (155, '2021-08-24', '2021-08-26', '2022-05-25', 'F', 0, 0, 0,0, 'Mitte der Wiese als Koordinate genommen, 6 Tiere festgestellt, 0, nur eines durch Foto belegt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (157, '2021-08-19', '2021-08-26', '2022-05-25', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (159, '2021-08-26', '2021-08-27', '2022-05-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (160, '2021-08-20', '2021-08-27', '2022-05-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (161, '2021-08-25', '2021-08-27', '2022-06-17', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (162, '2021-08-25', '2021-08-27', '2022-06-17', 'F', 0, 1, 0, 0, 0, 'Koordinaten gelliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (163, '2021-08-27', '2021-08-27', '2022-06-17', 'F', 0, 0, 0, 1, 0, 'Koordinaten in den Bildeigenschaften + Adresse geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (164, '2021-08-27', '2021-08-27', '2022-06-17', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (165, '2021-08-27', '2021-08-27', '2022-06-17', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (166, '2021-08-28', '2021-08-28', '2022-06-17', 'F', 1, 0, 0, 0, 'vermutlich sicherer Fund, Förster, 0, Melder hat bereits mit Foto gemeldet und das Foto des Melders gesehen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (167, '2021-08-28', '2021-08-28', '2022-06-17', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (168, '2021-08-28', '2021-08-28', '2022-06-17', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (169, '2021-08-28', '2021-08-28', '2022-06-17', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (170, '2021-08-29', '2021-08-29', '2021-06-17', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (171, '2021-08-24', '2021-08-29', '2021-06-17', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (172, '2021-08-29', '2021-08-29', '2022-06-17', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (173, '2021-08-30', '2021-08-30', '2022-06-17', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (174, '2021-08-29', '2021-08-30', '2022-06-17', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (175, '2021-08-24', '2021-08-30', '2022-06-17', 'F', 1, 0, 0, 0, 'Koordinaten geliefert, 0, Revierjagdmeiser Marcus Groeger');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (176, '2021-08-30', '2021-08-30', '2022-06-17', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (177, '2021-07-19', '2021-08-30', '2022-06-17', 'F', 1, 0, 0, 0, 'Koordinaten geliefert, Sicherer Fund, 0, Artenkenner');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (178, '2021-08-31', '2021-08-31', '2022-06-17', 'F', 0, 0, 0, 0, 0, 'insgesamt min 4 Tiere, 0, nur eins durch Foto belegt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (179, '2021-08-13', '2021-08-31', '2022-06-17', 'F', 0, 1, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (180, '2021-08-31', '2021-08-31', '2022-06-17', 'F', 1, 0, 0, 0, 0, 'sehr weitläufiger Hof mit Land Grünland dran, 0, Koordinaten vermutlich irgendwo im Grün');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (181, '2021-08-31', '2021-08-31', '2022-06-17', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (182, '2021-08-27', '2021-08-31', '2022-06-17', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (183, '2021-08-31', '2021-08-31', '2022-06-17', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (184, '2021-08-31', '2021-08-31', '2022-06-17', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (185, '2021-08-31', '2021-08-31', '2022-06-17', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (186, '2021-08-31', '2021-08-31', '2022-06-17', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (187, '2021-08-31', '2021-08-31', '2022-06-17', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (188, '2021-09-01', '2021-09-01', '2022-06-22', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (189, '2021-09-01', '2021-09-01', '2022-06-22', 'F', 0, 0, 1, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (190, '2021-09-01', '2021-09-01', '2022-06-22', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (191, '2021-09-01', '2021-09-01', '2022-06-22', 'F', 0, 0, 0, 0, 0, 'drei Tiere, 0, nur zwei durch Fotos belegt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (192, '2021-08-28', '2021-09-01', '2022-06-22', 'F', 1, 0, 0, 0, 0, 'keine weiteren FO-Daten vorhanden');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (193, '2021-09-01', '2021-09-01', '2022-06-22', 'F', 0, 1, 0, 0, 0, 'beim Nachbarn gibt es auch welche');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (194, '2021-08-26', '2021-09-02', '2022-06-22', 'F', 0, 0, 0, 1, 0, 'noch einmal nachgehakt 22.06.2022');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (195, '2021-09-01', '2021-09-02', '2022-06-22', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (196, '2021-09-02', '2021-09-02', '2022-06-22', 'F', 0, 0, 1, 0, 0, 'FO wie 20210928 Lauchhammer');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (197, '2021-09-02', '2021-09-02', '2022-06-22', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (198, '2021-09-02', '2021-09-02', '2022-06-22', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (199, '2021-09-02', '2021-09-02', '2022-06-22', 'F', 0, 0, 0, 1, 0, 'Koordinaten in Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (200, '2021-08-31', '2021-09-03', '2022-06-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (201, '2021-09-03', '2021-09-03', '2022-06-27', 'F', 0, 1, 0, 0, 0, 'Meldung eines Weibchens; Bemerkung: kleines Tier nicht durch Foto belegt, Männchen oder Nymphe, 0, Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (202, '2021-09-03', '2021-09-03', '2022-06-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (203, '2021-08-31', '2021-09-03', '2022-06-27', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (204, '2021-09-03', '2021-09-03', '2022-06-27', 'F', 0, 1, 0, 0, 0, 'FO siehe 20210724 Thyrow');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (205, '2021-09-02', '2021-09-03', '2022-06-27', 'F', 0, 1, 0, 0, 0, 'Koordinaten gelifert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (206, '2021-09-04', '2021-09-04', '2022-06-27', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (207, '2021-09-03', '2021-09-04', '2022-06-27', 4, 'F', 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (208, '2021-09-04', '2021-09-04', '2022-06-27', 'F', 0, 1, 0, 0, 0, 'auf dem Foto schlecht zu erkennen ob Weibchen oder weibliche Nymphe; Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (209, '2021-09-04', '2021-09-04', '2022-06-27', 'F', 1, 0, 0, 0, 0, 'FO siehe 20210731 Roddahn');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (210, '2021-07-19', '2021-09-04', '2022-06-27', 'F', 1, 0, 0, 0, 'Koordinaten geliefert, Finder ist Artenkenner, 0, somit sicherer Fund ohne belegfoto');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (211, '2021-09-04', '2021-09-04', '2022-06-27', 'F', 1, 0, 0, 0, 0,'Koordinaten geliefert, 0, im Wald');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (212, '2021-09-03', '2021-09-04', '2022-06-27', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (213, '2021-09-05', '2021-09-05', '2022-07-05', 'F', 0, 0, 0, 0, 'Auf dem Video ist ein Männchen, 2 Tiere im Flüg gemeldet, 0, vermutlich 2 Männchen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (214, '2021-09-02', '2021-09-05', '2022-07-05', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (215, '2021-09-03', '2021-09-05', '2022-07-05', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (216, '2021-09-05', '2021-09-05', '2022-07-05', 'F', 0, 0, 0, 0, 'Koordinaten geliefert, 9 Tiere (Männchen und Weibchen) gemeldet worden, 0, keine Ootheken');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (217, '2021-09-05', '2021-09-05', '2022-07-05', 'F', 0, 0, 0, 0, 0, 'FO siehe 20210702 Gottsdorf');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (219, '2021-09-05', '2021-09-05', '2022-07-05', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (220, '2021-09-05', '2021-09-05', '2022-07-05', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (221, '2021-09-05', '2021-09-05', '2022-07-05', 'F', 0, 1, 0, 0, 0, 'ohne Hausnummer nicht genauer zu bestimmen, 0, Genauigkeit100 m');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (222, '2021-09-04', '2021-09-05', '2022-07-05', 'F', 0, 0, 1, 0, 0, 'FO siehe 20211013 Trebbin OT Märkisch Wilmersdorf');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (223, '2021-09-02', '2021-09-05', '2022-07-07', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (224, '2021-09-06', '2021-09-06', '2022-07-07', 'F', 0, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (226, '2021-09-06', '2021-09-06', '2022-07-07', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (227, '2021-09-02', '2021-09-06', '2022-07-08', 'F', 0, 1, 0, 0, 0, 'Neubaugebiet, 0, Koordinaten über Google nicht eindeutig zuzuordnen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (229, '2021-09-05', '2021-09-06', '2022-07-08', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (230, '2021-09-07', '2021-09-09', '2022-07-08', 'F', 0, 1, 0, 0, 'Melderin hat mit Foto gemeldet, sicherer Fund, 0, Adresse Schießstand noch einmal recherchieren');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (231, '2021-09-03', '2021-09-06', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (232, '2021-09-05', '2021-09-06', '2022-07-08', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (233, '2021-09-05', '2021-09-06', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (234, '2021-09-01', '2021-09-06', '2022-07-08', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (235, '2021-09-06', '2021-09-06', '2022-07-08', 'F', 0, 0, 0, 1, 0, 'in Copula');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (236, '2020-09-20', '2021-09-06', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (237, '2021-09-06', '2021-09-06', '2022-07-08', 'F', 0, 1, 0, 0, 'Totfund, 0, Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (238, '2021-08-20', '2021-09-06', '2022-07-08', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (239, '2021-08-20', '2021-09-06', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (240, '2021-09-01', '2021-09-06', '2022-07-08', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (241, '2021-09-05', '2021-09-06', '2022-07-08', 'F', 0, 0, 0, 0, 0, 'Mehrfachmelderin, 5 Tiere ohne nähere Angaben, 0, siehe auch20210809 Waldstadt (Zossen)');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (242, '2021-09-06', '2021-09-06', '2022-07-08', 'F', 0, 0, 0, 0, 0, '1 Männchem mit Foto belegt, 3 Tiere gemeldet, 0, siehe auch20210809 Waldstadt (Zossen)');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (243, '2021-09-07', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, '100 m Luftlinie von den beiden vorherigen Funden, siehe 20210823 Baruth, 0, große Grünlandfläche');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (244, '2021-09-06', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (245, '2021-08-14', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (246, '2021-08-21', '2021-09-07', '2022-07-08', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (247, '2021-09-07', '2021-09-07', '2022-07-08', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (248, '2021-09-07', '2021-09-07', '2022-07-08', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (249, '2021-08-27', '2021-09-07', '2022-07-08', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (250, '2021-09-07', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (251, '2021-09-07', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (252, '2021-08-16', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (253, '2021-08-20', '2021-09-07', '2022-07-08', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (254, '2021-08-25', '2021-09-07', '2022-07-08', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (255, '2021-08-20', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (256, '2021-09-05', '2021-09-07', '2022-07-08', 'F', 0, 1, 0, 0, 0, 'regelmäßige Sichtungen auch auf dem Nachbargrundstück Elsterwerdaer Straße 41');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (257, '2021-08-29', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (259, '2021-09-05', '2021-09-07', '2022-07-08', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (260, '2021-09-03', '2021-09-07', '2022-07-08', 'F', 0, 1, 0, 0, 'Totfund 5 cm lang, 0, entweder ein Männchen oder eine Nymphe');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (261, '2021-09-17', '2021-10-24', '2022-07-08', 'F', 1, 0, 0, 0, 'Totfund ohne Foto und Geschlechtsangaben, 0, 1 lebendes Männchen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (262, '2021-09-06', '2021-09-07', '2022-07-08', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (263, '2021-09-07', '2021-09-07', '2022-07-08', 'F', 0, 1, 0, 0, 0, 'weggeflogen, 0, vielleicht ein Männchen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (264, '2021-09-05', '2021-09-07', '2022-07-08', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert (siehe 20210912 Gorden)');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (265, '2021-09-03', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (266, '2021-08-29', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (267, '2021-08-31', '2021-09-07', '2022-07-08', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (268, '2021-09-05', '2021-09-07', '2022-07-08', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (269, '2021-09-07', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (270, '2021-08-26', '2021-09-07', '2022-07-08', 'F', 0, 0, 1, 0, 0, 'Koordinaten nicht genauer bestimmbar');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (271, '2021-09-07', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (272, '2022-09-04', '2021-09-07', '2022-07-08', 'F', 0, 0, 0, 1, 0, 'vielleicht das selbe Tier wie 20210906 Lieberose Heide (Generalshügel), 0, vermutlich ein Weibchen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (273, '2021-09-05', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (274, '2021-09-02', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (275, '2021-08-28', '2021-09-07', '2022-07-08', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (276, '2021-08-14', '2021-09-07', '2022-07-08', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (277, '2021-08-28', '2021-09-07', '2022-07-08', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (278, '2021-09-07', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (279, '2021-09-05', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (280, '2021-09-07', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (281, '2021-09-05', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (283, '2021-09-11', '2021-09-11', '2022-07-08', 'F', 0, 0, 0, 0,'3 Tiere gemeldet, eins davon mit Foto, Geschlecht auf dem Foto nicht sicher bestimmbar (unscharf), 0, Imago');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (284, '2021-09-02', '2021-09-07', '2022-07-08', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (286, '2021-08-14', '2021-09-07', '2022-07-08', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (287, '2021-09-07', '2021-09-07', '2022-07-08', 'F', 0, 1, 0, 0, 'schnell weggeflogen, somit Imago, 0, Geschlecht vermutllich ein Männchen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (288, '2021-08-22', '2021-09-07', '2022-07-08', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (289, '2021-09-06', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (290, '2021-09-04', '2021-09-07', '2022-07-08', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (291, '2021-08-29', '2021-09-07', '2022-07-22', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (292, '2021-09-07', '2021-09-07', '2022-07-22', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (293, '2021-09-04', '2021-09-07', '2022-07-22', 'F', 0, 0, 1, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (294, '2021-09-05', '2021-09-07', '2022-07-22', 'F', 0, 1, 0, 0, 'schon seit einigen Jahren ansässig, ein Tier angegeben, 0, Geschlecht nicht bekannt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (295, '2021-09-01', '2021-09-07', '2022-07-25', 'F', 1, 0, 0, 0, 0, 'bei Manfred K. nachgehakt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (296, '2021-09-06', '2021-09-07', '2022-07-25', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (297, '2021-09-06', '2021-09-07', '2022-07-25', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (298, '2021-08-14', '2021-09-07', '2022-07-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (300, '2020-09-20', '2021-09-07', '2022-07-25', 'F', 1, 0, 0, 0, 0, 'FO siehe 20200918 Vetschau');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (301, '2021-09-06', '2021-09-07', '2022-07-25', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (302, '2019-08-31', '2021-09-07', '2022-07-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (304, '2021-09-07', '2021-09-07', '2022-07-25', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (305, '2021-09-09', '2021-09-09', '2022-07-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (306, '2021-09-05', '2021-09-07', '2022-07-25', 'F', 0, 1, 0, 0, 'Koordinaten geliefert, Mehrfachmelderin (Nymphen am 22.08.2021), 0, auch schono mit Fotos');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (307, '2021-08-18', '2021-09-07', '2022-07-25', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (308, '2021-09-05', '2021-09-07', '2022-07-25', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (309, '2021-08-25', '2021-09-08', '2022-07-25', 'F', 0, 0, 0, 1, 'Koordinaten geliefert, 0, a');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (310, '2021-09-08', '2021-09-08', '2022-07-25', 'F', 0, 1, 0, 0, 'Koordinaten geliefert, 0, a');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (311, '2021-09-07', '2021-09-08', '2022-07-25', 'F', 1, 0, 0, 0, 0, 'Fundort Christinendorfer Allee 6 genaue Verortung der verschiedenen Weideflächen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (312, '2021-09-08', '2021-09-08', '2022-07-25', 'F', 0, 1, 0, 0, 'Angabe ein Tier, Mehrfachmelderein, 0, schon Tier mit');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (313, '2021-09-08', '2021-09-08', '2022-07-25', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (314, '2021-09-08', '2021-09-08', '2022-07-25', 'F', 1, 0, 0, 0, 'Punkt in eine sehr grobe Karte gesetzt, 0, müsste in der Nähe Klarastraße liegen ');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (315, '2021-09-08', '2021-09-08', '2022-07-25', 'F', 0, 1, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (316, '2021-08-25', '2021-09-08', '2022-07-25', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (317, '2021-09-08', '2021-09-08', '2022-07-25', 'F', 0, 0, 1, 0, 0, 'Koordinaten gliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (318, '2021-09-08', '2021-09-08', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (319, '2021-09-07', '2021-09-08', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (320, '2021-08-14', '2021-09-08', '2022-07-26', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (321, '2021-09-07', '2021-09-08', '2022-07-26', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (322, '2021-09-08', '2021-09-08', '2022-07-26', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (323, '2021-08-30', '2021-09-08', '2022-07-26', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (324, '2021-08-23', '2021-09-08', '2022-07-26', 'F', 0, 1, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (325, '2021-09-07', '2021-09-08', '2022-07-26', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (326, '2021-09-08', '2021-09-08', '2022-07-26', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (327, '2021-09-06', '2021-09-08', '2022-07-26', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (328, '2021-09-08', '2021-09-08', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (329, '2021-09-08', '2021-09-08', '2022-05-25', 'F', 0, 0, 1, 0, 0, 'Meldung mit Foto (20210827 Cottbus (Gallinchen)); FO westl. des Flughafens ggf. etwas ungenau');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (330, '2021-09-06', '2021-09-08', '2022-07-26', 'F', 1, 0, 0, 0, 'Foto bei den Manfreds nachgehakt, 0, bei Manfred B. liegt keins mehr vor');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (332, '2021-09-08', '2021-09-08', '2022-07-26', 'F', 0, 0, 0, 1, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (334, '2022-09-08', '2021-09-08', '2022-07-26', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (335, '2022-09-08', '2021-09-08', '2022-07-26', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (336, '2021-09-08', '2021-09-08', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (337, '2021-09-07', '2021-09-08', '2022-07-26', 'F', 0, 1, 0, 0, 0, 'FO siehe 20210907 Völklingen Saarland');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (338, '2021-09-06', '2021-09-08', '2022-07-26', 'F', 0, 0, 1, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (339, '2021-08-24', '2021-09-09', '2022-07-26', 'F', 0, 0, 0, 1, 0, 'siehe auch siehe auch 20210920 Altdöbern');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (340, '2021-09-08', '2021-09-09', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (341, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (342, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 0, 1, 0, 0, 0, 'bereits 2020 wurde hier ein Tier gesehen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (343, '2021-09-05', '2021-09-09', '2022-07-26', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (344, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (345, '2021-08-15', '2021-09-09', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (346, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (347, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (348, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (349, '2021-09-06', '2021-09-09', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (350, '2021-09-04', '2021-09-09', '2022-07-26', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (351, '2021-08-24', '2021-09-09', '2022-07-26', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (352, '2021-09-08', '2021-09-09', '2022-07-26', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (353, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 0, 0, 1, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (354, '2021-09-03', '2021-09-09', '2022-07-26', 'F', 0, 1, 0, 0, 0, 'Foto whats App muss bei Manfred sein, 0, bei Manfred angefragt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (355, '2021-09-06', '2021-09-09', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (356, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (357, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 0, 0, 0, 1, 'Koordinaten nicht genauer bestimmbar, 0, auch die Nachfrage bei den Findern ergab keine Hausnummer');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (358, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (359, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (360, '2021-09-08', '2021-09-09', '2022-07-26', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (361, '2021-09-09', '2021-09-09', '2022-07-26', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (362, '2021-08-29', '2021-09-09', '2022-09-26', 'F', 0, 0, 1, 0, 0, 'Artenkenner, sicherer Fund, 0, Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (363, '2021-09-09', '2021-09-09', '2022-09-26', 'F', 0, 0, 0, 0, 0, 'insgesamt 4 Tiere die letzten Tage, 0, ein Tier mit Foto belegt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (364, '2021-09-05', '2021-09-10', '2022-09-26', 'F', 0, 0, 0, 0, 0, '7/8 Tiere angegeben, 3 Tier durch Foto belegt, 0, Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (366, '2021-09-10', '2021-09-10', '2022-09-26', 'F', 0, 0, 0, 1, 'Finder Ehemann der Tochter, 0, Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (367, '2021-09-09', '2021-09-10', '2022-09-26', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (368, '2021-09-10', '2021-09-10', '2022-09-26', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (369, '2021-08-21', '2021-09-10', '2022-09-26', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (370, '2021-09-10', '2021-09-10', '2022-09-26', 'F', 0, 0, 1, 0, 0, 'Hausnummer aus Telefonbuch ');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (371, '2020-10-30', '2021-09-10', '2022-09-26', 'F', 0, 1, 0, 0, 0, 'Koordinaten gelilefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (372, '2021-09-10', '2021-09-10', '2022-09-26', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (373, '2021-09-09', '2021-09-10', '2022-09-26', 'F', 1, 0, 0, 0, 0, 'Größenangabe 5-6 cm und schnell davongeflogen, Männchen, 0, Finderin: Nachbarin');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (376, '2021-09-10', '2021-09-10', '2022-09-27', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (377, '2021-09-10', '2021-09-10', '2022-09-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (378, '2021-09-09', '2021-09-10', '2022-09-27', 'F', 1, 0, 0, 0, 'Koordinaten geliefert, Finder: Kollegen, 0, Zeitangabe in den letzten Tagen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (379, '2021-09-10', '2021-09-10', '2022-09-27', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (380, '2021-09-10', '2021-09-10', '2022-09-27', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (381, '2021-09-08', '2021-09-10', '2022-09-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (382, '2021-09-10', '2021-09-10', '2022-09-27', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (383, '2021-09-03', '2021-09-10', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (384, '2021-09-10', '2021-09-10', '2022-09-27', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (385, '2021-09-09', '2021-09-10', '2022-09-27', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (386, '2021-09-09', '2021-09-10', '2022-07-26', 'F', 1, 0, 0, 0, 0, 'Finder. Schwager');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (387, '2021-09-01', '2021-09-10', '2022-07-26', 'F', 0, 0, 1, 0, 0,'Mehrfachmelder, auch mit Fotos, kennt die Art, 0, sicherer Fnd');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (388, '2021-09-07', '2021-09-10', '2022-07-26', 'F', 1, 0, 0, 0, 0, 'Mehrfachmelder, auch mit Fotos, kennt die Art, 0, sicherer Fnd');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (389, '2021-09-08', '2021-09-10', '2022-07-26', 'F', 1, 0, 0, 0, 0, 'Mehrfachmelder, auch mit Fotos, kennt die Art, 0, sicherer Fnd');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (390, '2021-09-10', '2021-09-10', '2022-09-27', 'F', 0, 0, 0, 1, 0, 'Fundmunkt nicht auf den Meter genau');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (391, '2021-09-10', '2021-09-10', '2022-09-27', 'F', 0, 1, 0, 0, 0, 'Artenkenner, sicherer Fund, 0, Koordinaten nicht genau');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (392, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'lt. Ebay-Kleinanzeigen gibt es einen Toni Krobock in Senftenberg Großenhainerstraße 17, 0,  01968 Brandenburg');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (393, '2021-07-01', '2021-09-11', '2022-09-27', 'F', 0, 1, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (394, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (395, '2021-08-19', '2021-09-11', '2022-09-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (396, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (397, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften, 0, 2 Männchen auf einem Weibchen 1x kopflos');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (398, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (399, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'FO siehe 20210912 Lauchhammer');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (400, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 0, 0, 1, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (401, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (402, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (403, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert, 0, an welchem der beiden Punkte der Meldung das erwachsene Weibchen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (404, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (405, '2021-09-09', '2021-09-11', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (406, '2021-08-02', '2021-09-11', '2022-09-27', 'F', 0, 0, 0, 1, 0, 'ggf. das gleiche Tier');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (408, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (409, '2021-09-11', '2021-09-11', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (410, '2021-09-13', '2021-09-12', '2022-09-27', 'F', 0, 1, 0, 0, '3-4 cm, 0, Nymphe');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (411, '2021-09-12', '2021-09-12', '2022-09-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (412, '2021-09-12', '2021-09-12', '2022-09-27', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (413, '2021-09-04', '2021-09-12', '2021-09-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (414, '2021-09-12', '2021-09-12', '2021-09-27', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (415, '2021-09-12', '2021-09-12', '2021-09-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (416, '2021-08-28', '2021-09-12', '2022-09-27', 'F', 0, 1, 0, 0, 'Mehrfachmelder, auch mit Fotos, kennt die Art, sicherer Fund, 0, Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (417, '2021-09-10', '2021-09-12', '2021-09-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (419, '2021-09-12', '2021-09-12', '2021-09-27', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (420, '2021-09-10', '2021-09-12', '2021-09-27', 2, 'F', 0, 0, 0, '2 Tiere, nur ein Weibchen mit Bild belegt, 0, Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (421, '2021-09-09', '2021-09-12', '2021-09-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (422, '2021-09-12', '2021-09-12', '2021-09-27', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (423, '2021-09-12', '2021-09-12', '2021-09-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (424, '2021-09-12', '2021-09-12', '2022-09-27', 'F', 0, 0, 0, 1, 0, 'vielleicht die leichen Tiere wie in 20210911 Lauchhammer');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (425, '2021-09-12', '2021-09-12', '2022-09-27', 'F', 1, 0, 0, 0, 'sehr verwirrend, mal Männchen, 0, mal Weibchen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (426, '2021-09-12', '2021-09-12', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'Finder: Nachbar');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (427, '2021-09-12', '2021-09-12', '2022-09-27', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (428, '2021-09-11', '2021-09-12', '2022-09-27', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (429, '2021-09-12', '2021-09-12', '2022-09-27', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (430, '2021-09-12', '2021-09-12', '2022-09-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (431, '2021-10-02', '2021-09-12', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'Finderin: Schwiegermutter');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (432, '2021-09-11', '2021-09-12', '2022-09-27', 'F', 1, 0, 0, 0, 'Koodiinaten geliefert, 0, aber auch in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (433, '2021-09-12', '2021-09-12', '2022-09-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (434, '2021-09-12', '2021-09-12', '2022-09-27', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (435, '2021-09-13', '2021-09-13', '2022-09-27', 'F', 0, 0, 1, 0, 0, 'Kennt die Tiere aus Italien');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (436, '2021-09-12', '2021-09-13', '2022-09-27', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (437, '2021-09-13', '2021-09-13', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (438, '2021-09-12', '2021-09-13', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (439, '2021-09-11', '2021-09-13', '2022-09-27', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (440, '2021-09-13', '2021-09-13', '2022-09-27', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (441, '2021-09-12', '2021-09-13', '2022-09-27', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (442, '2021-09-13', '2021-09-13', '2022-09-27', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (443, '2021-09-09', '2021-09-13', '2022-09-27', 'F', 0, 1, 0, 0, 0, 'Koordiniaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (444, '2021-09-09', '2021-09-13', '2022-09-27', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (445, '2021-09-13', '2021-09-13', '2022-07-29', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (446, '2021-09-13', '2021-09-13', '2022-07-29', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (447, '2021-09-12', '2021-09-13', '2022-07-29', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (448, '2021-09-13', '2021-09-13', '2022-07-29', 'F', 0, 0, 1, 0, 'Koordinaten sehr ungenau, 0, keine weitere Angabe von der Melderin auf Anfrage.');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (449, '2021-09-13', '2021-09-13', '2022-07-29', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (450, '2021-09-06', '2021-09-13', '2022-07-29', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (451, '2021-09-10', '2021-09-13', '2022-07-29', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert, prüfen, 0, ob die Daten von Kunick publiziert worden sind');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (452, '2021-09-10', '2021-09-13', '2022-07-29', 'F', 0, 1, 0, 0, 'Koordinaten geliefert, prüfen, 0, ob die Daten von Kunick publiziert worden sind');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (453, '2021-09-13', '2021-09-13', '2022-07-29', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (454, '2021-09-01', '2021-09-13', '2022-07-29', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (455, '2021-09-13', '2021-09-13', '2022-07-29', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (456, '2021-07-23', '2021-09-13', '2022-07-29', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (457, '2022-08-21', '2021-09-13', '2022-08-03', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (458, '2021-09-11', '2021-09-13', '2022-08-03', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (459, '2021-09-13', '2021-09-13', '2022-08-03', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (460, '2021-08-13', '2021-09-13', '2022-08-03', 'F', 0, 1, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (461, '2021-09-13', '2021-09-13', '2022-08-03', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (462, '2021-09-13', '2021-09-13', '2022-08-03', 'F', 0, 0, 1, 0, 0, 'Koordinaten siehe 20210904 Zossen OT Wünsdorf');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (463, '2021-09-13', '2021-09-14', '2022-08-03', 'F', 1, 0, 0, 0, 'Angabe 3-4 cm lang, 0, somit Nymphe');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (464, '2021-09-14', '2021-09-14', '2022-08-03', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (465, '2021-09-11', '2021-09-14', '2022-08-03', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (466, '2021-09-12', '2021-09-14', '2022-08-03', 'F', 0, 0, 0, 1, 0, 'en Copula');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (467, '2021-09-14', '2021-09-14', '2022-08-03', 'F', 1, 0, 0, 0, 0, 'FO siehe 20210901 Luckau OT Görlsdorf');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (468, '2021-09-12', '2021-09-14', '2022-08-03', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (469, '2021-08-18', '2021-09-14', '2022-08-03', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (470, '2021-09-25', '2021-10-13', '2022-08-03', 'F', 1, 0, 0, 0, 'Mehrfachmelderin auch mit Foto, 0, sichere Fund');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (471, '2021-09-13', '2021-09-14', '2022-08-03', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (472, '2021-09-10', '2021-09-14', '2022-08-03', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (473, '2021-09-11', '2021-09-14', '2022-08-03', 'F', 1, 0, 0, 0, 0, '15 Tiere gemeldet, eins durch Foto belegt, 0, Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (474, '2021-09-14', '2021-09-14', '2022-08-03', 'F', 0, 0, 1, 0, 0, 'FO siehe 20210709 Nierstein Larve, 0,  Gottesanbeterin 20200826 Nierstein mg ');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (475, '2021-09-03', '2021-09-14', '2022-08-03', 'F', 0, 1, 0, 0, 0, 'Entomologen der Berliner Fachgruppe, sicherer Fund, 0, Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (476, '2021-08-13', '2021-09-14', '2022-08-03', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (477, '2021-09-13', '2021-09-14', '2022-08-03', 'F', 0, 0, 0, 1, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (478, '2021-09-13', '2021-09-14', '2022-08-03', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (479, '2021-09-13', '2021-09-14', '2022-08-03', 'F', 0, 1, 0, 0, 0, 'evtl. publiziert?');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (480, '2021-09-14', '2021-09-14', '2022-08-03', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (481, '2021-09-14', '2021-09-14', '2022-08-03', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (482, '2021-08-20', '2021-09-14', '2022-08-03', 'F',0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (483, '2021-08-20', '2021-09-14', '2022-08-03', 'F', 0, 0, 1, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (484, '2021-08-15', '2021-09-14', '2022-08-03', 'F', 0, 0, 0, 1, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (485, '2021-09-14', '2021-09-14', '2022-08-03', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (486, '2021-09-14', '2021-09-14', '2022-08-03', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (487, '2021-09-07', '2021-09-15', '2022-08-03', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (488, '2021-09-15', '2021-09-15', '2022-08-03', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (489, '2021-09-08', '2021-09-15', '2022-08-03', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (490, '2021-09-15', '2021-09-15', '2022-08-03', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (491, '2021-09-15', '2021-09-15', '2022-08-03', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (492, '2021-09-04', '2021-09-15', '2022-08-03', 'F', 0, 1, 0, 0, 0, 'Artenkenner, 0, sicherer Fund');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (493, '2021-09-15', '2021-09-15', '2022-08-03', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (494, '2021-09-15', '2021-09-15', '2022-08-03', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (495, '2021-09-12', '2021-09-15', '2022-08-03', 'F', 0, 0, 0, 0, 0,'3 Tiere gemeldet, 0, eins davon mit Foto belegt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (496, '2021-09-06', '2021-09-15', '2022-08-03', 'F', 0, 1, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (497, '2021-09-15', '2021-09-15', '2022-08-03', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (498, '2021-09-15', '2021-09-15', '2022-08-03', 'F', 1, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (499, '2021-09-12', '2021-09-15', '2022-08-03', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (500, '2021-09-15', '2021-09-15', '2022-08-03', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (501, '2021-09-15', '2021-09-15', '2022-08-03', 'F', 0, 1, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (502, '2021-09-14', '2021-09-15', '2022-08-10', 'F', 1, 0, 0, 0, 0, 'Koordinaten in den Bildeigenschaften');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (503, '2021-09-12', '2021-09-16', '2022-08-10', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (504, '2021-09-16', '2021-09-16', '2022-08-10', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (505, '2021-09-14', '2021-09-16', '2022-08-10', 'F', 0, 1, 0, 0, 0, '1x en copula, 0, Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (506, '2021-09-16', '2021-09-16', '2022-08-10', 'F', 0, 0, 0, 1, 0, 'zu einer Nabu-Veranstaltung mitgenommen und im Garten der Ökolaube am Schlaatz davon geflogen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (507, '2020-08-01', '2021-09-16', '2022-08-10', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (508, '2021-09-15', '2021-09-16', '2022-08-10', 'F', 0, 1, 0, 0, 0, 'keine weiteren Informationen zum Fund');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (509, '2021-09-16', '2021-09-16', '2022-08-10', 'F', 1, 0, 0, 0, 0, 'FO in Karte geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (510, '2021-09-16', '2021-09-16', '2022-08-10', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (511, '2021-08-14', '2021-09-16', '2022-08-10', 'F', 0, 0, 1, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (513, '2021-09-13', '2021-09-16', '2022-08-10', 'F', 0, 0, 0, 1, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (514, '2021-09-16', '2021-09-16', '2022-08-10', 'F', 1, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (515, '2021-09-17', '2021-09-17', '2022-08-10', 'F', 0, 1, 0, 0, 0, 'Halb-Totfund (auf dem Bild tot, 0, lt. Melder beim Auffinden schon sehr erschöft)');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (516, '2021-06-06', '2021-06-10', '2021-06-10', 'E', 0, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (517, '2021-06-09', '2021-06-09', '2021-07-30', 'E', 0, 0, 0, 0, 0, 'Koordinaten gemessen');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (518, '2019-09-15', '2021-06-09', '2021-06-10', 'E', 0, 0, 0, 0, 0, 'Koordinaten geliefert, da die Meldung erst 2021 kam, 0, wurde Sie unter Fundmeldungen 2021 abgelegt');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (521, '2021-06-19', '2021-06-20', '2021-07-30', 'E', 0, 0, 0, 0, 0, 'Entomologin');
INSERT INTO meldungen (id, dat_fund_von, dat_fund_bis, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (522, '2021-06-20', '2021-06-23', '2021-06-20', '2021-07-30', 'E', 0, 0, 0, 0, 0, 'Spezialist');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (530, '2021-08-06', '2021-08-09', '2021-08-09', 'E', 0, 0, 0, 0, 0, 'Es müsste ein Foto geben');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (531, '2021-08-12', '2021-08-12', '2021-08-13', 'E', 0, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (532, '2021-08-12', '2021-08-12', '2021-08-13', 'E', 0, 0, 0, 0, 0, 'Koordinaten geliefert');
INSERT INTO meldungen (id, dat_fund_von, dat_fund_bis, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (533, '2021-09-05', '2021-09-07', '2021-09-09', '2021-09-09', 'E', 0, 0, 0, 0, 0, NULL);
INSERT INTO meldungen (id, dat_fund_von, dat_meld, dat_bear, fo_quelle, art_m, art_w, art_n, art_o, art_s, anm_melder) 
VALUES (537, '2021-07-22', '2021-10-08', '2021-10-08', 'Z', 0, 1, 0, 0, 0, 'Koordinaten und Geschlecht am 08.10. bei den Autoren angefragt');
