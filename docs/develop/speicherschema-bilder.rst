Bildablage-System
=================
.. index:: Ablage; Bilder
.. index:: Bilder; Ablage
    
Speicherschema
--------------
- Bilder werden nach Datum in Ordnern gespeichert.
- Der Bildname enthält den Ort und die ID des Melders (Beispiel mit xxxx und yyyy).
- Bei Mehrfachmeldungen wird eine laufende Nummer angehängt.

::

    IMAGES
      │
      ├───2023-05-10
      │       caputh-yyyy-01.jpg
      │       caputh-yyyy-02.jpg
      │       potsdam-xxxx.jpg
      │
      └───2023-05-11
              brandenburg-xxxx-01.jpg
              brandenburg-xxxx-02.jpg
              potsdam-xxxx.jpg
